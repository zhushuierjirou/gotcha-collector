import json

import tweepy
import pymysql.cursors
import datetime
from flask import Flask
from flask import request
from tweepy import TweepError

app = Flask(__name__)


def init():
    global api
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)


def fetch_all():
    connection = pymysql.connect(host='localhost', user='root', password='12345678', database='gotcha',
                                 cursorclass=pymysql.cursors.DictCursor)
    with connection:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT * FROM `authors`"
            cursor.execute(sql)
            results = cursor.fetchall()
            for result in results:
                fetch_one(connection, connection.cursor(), result['name'], result['id'])


def fetch_author(name, author_id):
    connection = pymysql.connect(host='localhost', user='root', password='', database='gotcha',
                                 cursorclass=pymysql.cursors.DictCursor)
    with connection:
        with connection.cursor() as cursor:
            fetch_one(connection, cursor, name, author_id)


def fetch_one(connection, cursor, name, author_id):
    tweets = api.user_timeline(name, tweet_mode='extended', count=100)
    for tweet in tweets:
        print(tweet._json)
        if tweet._json.get('retweeted_status') is not None:
            continue
        try:
            sql = "INSERT INTO `messages` (`ext_id`, `text`, `media_url`, `media_type`, `posted_at`, `author_id`, `created_at`, `updated_at`) " \
                  "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
            date = datetime.datetime.strptime(tweet._json['created_at'], '%a %b %d %H:%M:%S %z %Y')
            now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            cursor.execute(sql, (tweet._json['id']
                                 , tweet._json['full_text']
                                 , tweet._json['entities'].get('media') and tweet._json['entities']['media'][0][
                                     'media_url']
                                 , tweet._json['entities'].get('media') and tweet._json['entities']['media'][0]['type']
                                 , date.strftime('%Y-%m-%d %H:%M:%S')
                                 , author_id
                                 , now
                                 , now
                                 ))
            connection.commit()
        except Exception as e:
            print(e)


def check(name):
    try:
        user = api.get_user(name)
    except TweepError as te:
        return 'error'
    return json.dumps(user._json)


init()


@app.route("/")
def web_index():
    return 'ok'


@app.route("/fetch_all")
def web_fetch_all():
    fetch_all()
    return 'ok'


@app.route("/fetch_one")
def web_fetch_one():
    name = request.args.get('name', '')
    author_id = request.args.get('author_id', '')
    fetch_author(name, author_id)
    return 'ok'


@app.route("/check")
def web_check():
    name = request.args.get('name', '')
    return check(name)


if __name__ == '__main__':
    app.run()
